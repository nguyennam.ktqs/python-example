# def elcom(a, b, c, d):
#     print(a)
#     print(b)
#     print(c)
#     print('end', d)
#
#
# lst = ['123', 'elcom', 69.96, 'namnh']
# elcom(lst[0], lst[1], lst[2], lst[3])
# elcom(*lst)

# ex2
# lst = ['123', 'elcom', 3]
# lst2 = '123'
# def a(s, d, *, r='kter'):
#     print(s, d)
# a(*lst, r=lst2)

# ex3
# def elcom(*args, kter):
#     print(args)
#     print(type(args))
#     print(kter)
#
# elcom(*(x for x in range(70)), kter='Namnh')

# ex4
# ==> phải truyền vào đúng tên key thì mới nhận đơcj biến
# unpacking
# def kteam(name, member):
#     print(name)
#     print(member)
#
# dic = {'name': 'Kteam', 'member': 69}
# kteam(**dic)

# ex5: packing
# def elcom(**kwargs):
#     print(kwargs)
#     print(type(kwargs))
#
# elcom(name='ELCOM', age='25 Năm')

# ex6:
# def test(*pr1, **pr2):
#     print(pr1)
#     print(pr2)
#
#
# pr2 = ['123', 'elcom', 69.96, 'namnh']
# test(*(pr2), name='ELCOM', age='25 Năm')
