n = int(input('Enter size of matrix: ')) # nhập số n
dx, dy = 1, 0 # khởi tạo biến dx, dy
x, y = 0, 0 # khởi tạo biến x, y
spiral_matrix = [[None] * n for j in range(n)] # tạo ma trận rỗng
for i in range(n ** 2): # vòng lặp từ 0 đến n^2
    spiral_matrix[x][y] = i # gán giá trị i vào ma trận
nx, ny = x + dx, y + dy # gán giá trị x + dx, y + dy vào nx, ny
if 0 <= nx < n and 0 <= ny < n and spiral_matrix[nx][ny] == None: # nếu nx, ny nằm trong ma trận và giá trị tại nx, ny là None
    x, y = nx, ny # gán giá trị nx, ny vào x, y
else: # ngược lại
    dx, dy = -dy, dx # đổi giá trị dx, dy
x, y = x + dx, y + dy # gán giá trị x + dx, y + dy vào x, y
for y in range(n): # vòng lặp từ 0 đến n
    for x in range(n): # vòng lặp từ 0 đến n
        # print("%02i" % spiral_matrix[x][y], end=' ') # sửa lỗi in ra ma trận print("%02i" % spiral_matrix[x][y], end=' ')
        print(spiral_matrix[x][y], end=' ') # in ra ma trận
