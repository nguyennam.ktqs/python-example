# Bài 16 Kiểu dữ liệu set trong python
# Giới thiệu về Set trong Python

# q = {69,96}
# print(q)
# print(type(q))
#
# w = {1,2,3,4,5,5,6,7,7}
# print(w)
# w = w.clear()
# w = {}
# print(w)
# print(type(w))

# Sử dụng Set Comprehension
# set_1 = {i for i in range(10)}
# print(set_1)

# Sử dụng constructor Set
# set_1 = set((1, 2, 3))
# print(set_1)
# set2 = set('Chào mừng bạn')
# print(set2)
# {'ừ', 'C', 'ạ', ' ', 'b', 'g', 'h', 'à', 'o', 'm', 'n'}
# tại sao lại in ra kết quả như vậy
# vì set không chứa các phần tử trùng lặp
# nhưng nó không in theo thứ tự mà in theo thứ tự tăng dần của mã ASCII

# set3 = set((1,2,3,4))
# print(set3)
# set4 = set([1, 2, 2])
# print(set4)

# Một số toán tử với Set trong Python
# print(1 in {1, 2, 3}) # True
# print(4 in {'a', 'How Kteam', 5}) # False
# q = set((1, 2, 3)) - set((2, 3))
# print(q)

# w ={1, 2, 3} & {1, 4, 5} # giao của 2 tập hợp
# print(w)
# print({1, 2, 3} | {1, 2, 3}) # hợp của 2 tập hợp
# print({1, 2, 3} | {4, 5, 1}) # hợp của 2 tập hợp loại bỏ phần tử trùng nhau
# print({1, 2, 3} ^ {4, 5}) # lây phần tử không trùng nhau của 2 tập hợp


# Các phương thức của Set
# set_1 = {1, 2}
# set_1.clear()
# print(set_1)

# set_1 = {1, 2,3,4,6,5}
# a = set_1.pop() # trả về phần tử đầu tiên của tập hợp và xóa nó khỏi tập hợp
# a = set_1.pop()
# print(a)
# print(set_1.pop())
#
# set_1 = {1, 2,3,4,6,5}
# set_1.remove(5) # xóa phần tử 5 khỏi tập hợp
# set_1.remove(1) # xóa phần tử 5 khỏi tập hợp
# set_1.remove(7) # 7 không tồn tại trong tập hợp nên sẽ báo lỗi
# print(set_1)
# a = {1, 2,3,4,5,6}
# a.discard(2)
# a.discard(7) # không báo lỗi nếu phần tử không tồn tại trong tập hợp
# print(a)
# discard khacs với remove là nếu phần tử không tồn tại trong tập hợp thì nó sẽ không báo lỗi

# a = {1, 2}
# b = a.copy() #Trả về một bản sao của Set
# print(b)
# print(a)
# b = a
# a.add(4)
# print(a)
# print(b)

