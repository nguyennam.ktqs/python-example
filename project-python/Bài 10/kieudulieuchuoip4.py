# Bài 10: Các kiểu dữ liệu chuỗi trong Python

# 10.1 Các phương thức biến đổi

#<chuỗi>.capitalize() => Trả về một chuỗi với kí tự đầu tiên được viết hoa và viết thường tất cả những kí tự còn lại.

# q = 'elcoM'.capitalize()
# print(q)
#
# w = 'hello, Howkteam!'.capitalize()
# print(w)
#
# e = '  howKTEAM'.capitalize()
# print(e)

# <chuỗi>.upper() => Trả về một chuỗi với tất cả các kí tự được chuyển thành các kí tự viết hoa
# r = 'kter'.upper()
# print(r)
#
# t = 'HOW kteam'.upper()
# print(t)
#
# y = ' python'.upper()
# print(y)

# <chuỗi>.lower() =>Trả về một chuỗi với tất cả các kí tự được chuyển thành các kí tự viết thường

# u = 'FREE education'.lower()
# print(u)
# i = ' kTer'.lower()
# print(i)

# <chuỗi>.swapcase() => Trả về một chuỗi với các kí tự viết hoa được chuyển thành viết thường, các kí tự viết thường được chuyển thành viết hoa
# o = 'free EDUCATION'.swapcase()
# print(o)
# p = 'HoW kTeAm'.swapcase()
# print(p)


# <chuỗi>.title() => Trả về một chuỗi với định dạng tiêu đề, có nghĩa là các từ sẽ được viết hoa chữ cái đầu tiên, còn lại là viết thường
# a = 'share to be better'.title()
# print(a)
# s = 'FREE EDUCATION'.title()
# print(s)

# 10.2 Các phương thức định dạng

# <chuỗi>.center(width, [fillchar]) => Trả về một chuỗi được căn giữa với chiều rộng width.
# Nếu fillchar là None (không được nhập vào) thì sẽ dùng kí tự khoảng trắng để căn, không thì sẽ căn bằng kí tự fillchar.
# Một điều nữa là kí tự fillchar là một chuỗi có độ dài là 1.
# d = 'abc'.center(12);
# print(d)
# f =  'abc'.center(12,'+')
# print(f)
# g = 'abc'.center(12,' ')
# print(g)

#<chuỗi>.rjust(width, [fillchar]) =>  Cách hoạt động tương tự như phương thức center, có điều là căn lề phải
# h = 'kteam'.rjust(12, '*')
# print(h)
# j = 'kteam'.ljust(12, '*')
# print(j)


# 10.3 Các phương xử lý

#<chuỗi>.encode(encoding=’utf-8’, errors=’strict’) =>
# k = 'ố ồ'.encode();
# print(k)

# <kí tự nối>.join(<iterable>)Trả về một chuỗi bằng cách nối các phần tử trong iterable bằng kí tự nối. Một iterable có thể là một tuple, list,… hoặc là một iterator (Kteam sẽ giải thích khái niệm này ở các bài sau).

# j = '*'.join(['1', '2', '3']) # iterable ở đây là list ['1', '2', '3']
# print(j)
#
# l = '*'.join(('1', '2', '3')) # iterable ở đây là list ['1', '2', '3']
# print(l)

# <chuỗi>.replace(old, new, [count]) => Trả về một chuỗi với các chuỗi old nằm trong chuỗi ban đầu được thay thế bằng chuỗi new. Nếu count khác None (có nghĩa là ta cho thêm count) thì ta sẽ thay thế old bằng new với số lượng count từ trái qua phải.

# z = 'abc how abc kteam'.replace('abc', 'aaa')
# print(z)

# x = 'abc how abc kteam'.replace('a', 'AA')
# print(x)

# c = 'abc how abc kteam'.replace('abcd', 'AA')
# print(c)

# v = 'abc how abc kteam'.replace('abc', 'AA', 2)
# print(v)

# <chuỗi>.strip([chars]) => Trả về một chuỗi với phần đầu và phần đuôi của chuỗi được bỏ đi các kí tự chars. Nếu chars bị bỏ trống thì mặc định các kí tự bị bỏ đi là dấu khoảng trắng và các escape sequence. Một số escape sequence ngoại lệ như \a sẽ được encode utf-8. Tuy vậy, không có ảnh hưởng gì tới nội dung.
# b =  '  Kter   '.strip()
# print(b)

# n = '%%%%Kter%%%'.strip('%')
# print(n)

# m = 'cababHowbaaaca'.strip('abc')
# print(m)

# q = '   Kter   '.lstrip();
# print(q)
# ư = '%%%%Kter%%%'.lstrip('%')
# print(ư)
# e = 'cababKterbaaaca'.lstrip('abc')
# print(e)