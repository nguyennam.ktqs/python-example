# Kiểu dữ liệu Dict trong Python - Phần 2

# Các phương thức tiện ích

#copy
# d = {'team': 'Kteam', (1, 2): 69}
# d_2 = d.copy()
# print(d_2)
# print(id(d_2))
# print(d)
# print(id(d))
#clear

# d = {'team': 'Kteam', (1, 2): 69}
# d = d.clear()
# print(d)

# d = {'team': 'Kteam', (1, 2): 69}
# d = d.get('team')
# print(d)
# d = d.get('a','hahaha') # AttributeError: 'str' object has no attribute 'get'

# d = {'team': 'Kteam', (1, 2): 69}
# items = d.items()
# print(items)
# print(type(items))


# Phương thức keys
# d = {'team': 'Kteam', (1, 2): 69}
# keys = d.keys()
# print(keys)
# list_key = list(keys)
# print(list_key)
# print(list_key[-2])

# Phương thức values
# d = {'team': 'Kteam', (1, 2): 69}
# d = d.values()
#
# d = list(d)
# print(d)

# d = {'team': 'Kteam', (1, 2): 69}
# a = d.pop('team')
# print(a)
# print(d)


# d = {'team': 'Kteam', (1, 2): 69, 'a':'b',1:2}
# a = d.popitem()
# print(a)
# print(d)

# d = {'team': 'Kteam', (1, 2): 69}
# print(d.setdefault('team'))
# a = d.setdefault('key1','value1')
# print(a)