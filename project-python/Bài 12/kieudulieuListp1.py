# Bài 12 Kiểu dữ liệu List trong Python

# 12.1
# lst = list([1, 2, 3])
# print(lst)
# ls = [1, 2, 3]
# print(ls)
#
# str_lst = list('HOWKTEAM')
# print(str_lst)

# 12.2 Một số toán tử với List trong Python
# Toán tử +
# lst = [1, 2]
# lst += ['one', 'two']
# print(lst)
# lst += 'abc'
# print(lst)

# Toán tử *
# lst = list('KTER') * 2
# print(lst)
# a = [1, 2] * 3
# print(a)

# Toán tử in
# q = 'a' in [1, 2, 3]
# print(q)
# w = 'a' in ['a', 2, 3]
# print(w)
# e = 'a' in [['a'], 'b', 'c'] # chỉ có ['a'] thôi, không có 'a'
# print(e)

# 12.3 Indexing và cắt List trong Python
r = [1, 2, 'a', 'b', [3, 4]]
# print(r)
# print(r[0])
# print(r[-1]) #giai thich: r[-1] là phần tử cuối cùng của mảng r, nếu phần tử cuối cùng là mảng thì sẽ in ra mảng đó còn không thì in ra phần tử cuối cùng   [3, 4] là mảng cuối cùng của mảng r
# print(r[1:3])
# print(r[0:2])
# print(r[2:])
# print(r[::-1]) # giải thích: r[::-1] là cách viết ngắn gọn của r[0: -1: -1] nghĩa là in ra mảng r từ phần tử cuối cùng đến phần tử đầu tiên

# 12.4 Thay đổi nội dung List trong Python
# t = 'math'
# print(t[1])
# y = list(t)
# print(y)

# u  = [[1, 2, 3], [4, 5, 6]]
# print(u)
# print(u[0])
# print(u[1])
# print(u[0][0])
# print(u[0][-1])
# print(u[1][1])
# print(u[0][:2])
# print(u[1][:])

# Không được phép gán List này qua List kia nếu không có chủ đích
# i = [1,2,3,4,5,6,7]
# print(i)
# o = list(i) # phép gán này cho tách ra hai mảng độc lập không can thiệp vào cùng một mảng dữ liêu
# print(o)
# o[0] = 'Nguyen Hoài Nam'
# print(o)
# print(i)

# p = [1,2,3,4,5,6]
# print(p)
# a = p[:]
# a[0] = "Nguyen Hoai Nam"
# print(a)
# print(p)

# Bài tập củng cố
# 1.a
# a = list('abc')
# b = list(a)
# c = list(b)
# print(c)
# 1.b
# a = [1,2,3]
# b = [4]
# print(b)
# c = a + b
# print(c)
# 1.c
# print(list())
# print([0] * 3)
# 2.3
# s = 'aaaaaaaAAAAAaaa//123123//000000//&&TTT%%abcxyznontqfadf'
# s = s.split('&&')

# Cách 1
# index1 = s.find('&&')
# print(index1)
# s = s[(index1+2):-1]
# print(s)
# index2 = s.find('%%')
# splitString = s[index2:-1]
# print(splitString)
# s = s.split(splitString)
# print(s)
# print(s[0])

# Cách 2
# code = s.split('&&')[-1].split('%%')[0]
# print(s.split('&&')[-1])
