# ex1
# inter = (x for x in range(10))
# for value in inter: # muốn thêm key vào thì dùng items()
#     print(value)
# ex2
# howkteam = {'name': 'Kteam', 'kter': 69}
# a = howkteam.items()
# print(a)
# b = list(howkteam.items())
# print(b)
# for key, value in b:
#     print(key, '=>', value)
#
# for key, value in a:
#     print(key, '=>', value)
#
# for key, value in howkteam.items():
#     print(key, '=>', value)

# ex3
# string = 'Chào mừng bạn đến với thế giới của Python'
# for value in string:
#     if value == ' ':
#         break
#     print(value)
# string2 = string[:]+' Nam'
# for value in string2:
#     if value == ' ':
#         continue
#     else:
#         print(value)
