# Bài 15 Sự khác nhau về toán tử của Hashable object và Unhashable object trong Python

# Như Kteam đã từng đề cập ở các bài trước đây, mọi thứ trong Python xoay quanh các đối tượng, và các giá trị ở đây chính là một đối tượng. Tuy vậy vẫn để là <giá trị> để tránh gây khó hiểu.
#
# Công dụng: Theo định nghĩa về hàm id trong tài liệu của Python thì hàm này sẽ trả về một số nguyên (int hoặc longint).
#
# Giá trị này là một giá trị duy nhất và là hằng số không thay đổi suốt chương trình.
# Trong chi tiết bổ sung của CPython có nói giá trị trả về của hàm id là địa chỉ của giá trị (đối tượng) đó trong bộ nhớ.
# Cao siêu là thế, nhưng bạn hoàn toàn có thể nghĩ đơn giản, con số trả về đó như cái số nhà của bạn. Bạn ở đâu, thì số nhà của bạn cũng sẽ tương ứng.

# Giới thiệu cơ bản về hàm id

# n = 69
# s = 'How KTeam'
# lst = [1, 2]
# tup = (3, 4)
#
# print(id(n))
# print(id(s))
# print(id(lst))
# print(id(123))
# print(id('Free Education'))
#
# Toán tử là một phương thức
# Lặp lại thêm một lần nữa, mọi thứ xoay quanh Python toàn là hướng đối tượng. Cả các toán tử cũng thế!
# n = 69
# print(n +1)
# print(n.__add__(10))
# print(n.__sub__(11))
# print(n.__radd__(12))
# print(n.__rsub__(13))

# Khác biệt về toán tử Hash Object và Unhash Object
# s_1 = 'HowKteam'
# s_2 = 'Free Education'
# print(id(s_1)) #1850943748656
# print(id(s_2)) #1850943830896
# s_1 = s_1 + ' Python'
# s_2 += ' Python'
# print(id(s_1)) #1850944104944
# print(id(s_2)) #1850943773952
# print(s_1)
# print(s_2)
# =>Ta cũng thấy, 2 toán tử = + cũng không có gì khác biệt lắm so với +=.
