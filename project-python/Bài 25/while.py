# ex1
# k = 0
# while k < 3000:
#     print('value of k : ', k)
#     k += 1
#     if k == 3:
#         print('k=3 thoat khoi vong lap')
#         break
# else:
#     print('k is not less than 3')

# ex2====================================================

# file_object = open('./draft.txt', 'r+')
# data = list(file_object)
# print(data)
# print('0.Đọc file')
# print(data)
# lenData = len(data)
# k = 0
# string = ['kteam', 'Kteam']
# string2 = 'ELCOM'
# print('1.Tìm kiếm ký tự [\'kteam\', \'Kteam\'] trong file draft.txt')
# while k < lenData:
#     if string[0] in data[k]:
#         print('kteam xuất hiện trong chuỗi: %s' % (data[k]))
#         data[k] = data[k].replace(string[0], string2)
#     if string[1] in data[k]:
#         print('kteam xuất hiện trong chuỗi: %s' % (data[k]))
#         data[k] = data[k].replace(string[1], string2)
#     k += 1
# else:
#     print('Đọc hết file')
#
# print('2. Nôi dung file sau khi thay đổi')
# print(data)

# using list comprehension
# listToStr = ' '.join([str(elem) for elem in data])
# print(listToStr)
# print('3.Ghi file sau khi thay đổi')
# file_object_change = open('./draft.txt', 'w+')
# file_object_change.write(listToStr)
# print('4.Đóng file')
# file_object_change.close()


# ex3====================================================

#
# data = [56, 14, 11, 756, 34, 90, 11, 11, 65, 0, 11, 35]
# # print(data)
# arrayLength = len(data)
# i = 0
# arraySort = []
# countNumber = []
# listItem = []
# # tách ra các mảng con để sắp xếp
# while i < arrayLength: # giai thích cho vòng lặp này là nếu phần tử không phải là 11 thì sẽ thêm vào mảng con
#     if data[i] != 11: # nếu phần tử là 11 thì sẽ tách mảng con ra
#         print(data[i])
#         listItem.append(data[i]) # thêm phần tử vào mảng con
#         if i == (arrayLength - 1): # nếu phần tử cuối cùng không phải là 11 thì sẽ thêm mảng con vào mảng chính
#             arraySort.append(listItem) # thêm mảng con vào mảng chính
#             listItem = [] # reset mảng con
#     else:
#         if len(listItem) > 0: # nếu mảng con có phần tử thì sẽ thêm vào mảng chính
#             arraySort.append(listItem) # thêm mảng con vào mảng chính
#             listItem = [] # reset mảng con
#         countNumber.append(i) # thêm vị trí của phần tử 11 vào mảng
#     i += 1
#
# # Sắp xếp mảng con
# j = 0
# lenghArraySort = len(arraySort)
# while j < lenghArraySort: # giai thích cho vòng lặp này là nếu mảng con có nhiều hơn 1 phần tử thì sẽ sắp xếp
#     if len(arraySort[j]) > 1: # nếu mảng con có nhiều hơn 1 phần tử thì sẽ sắp xếp
#         arraySort[j].sort() # sắp xếp mảng con
#     j += 1 # tăng biến đếm
#
# # Gộp mảng sau khi sắp xếp
# q = 0
# arrayTemplate = []
# while q < len(arraySort): # giai thích cho vòng lặp này là nếu mảng con có nhiều hơn 1 phần tử thì sẽ sắp xếp
#     print(arraySort[q]) # in ra mảng con
#     if q > 0: # nếu mảng con có nhiều hơn 1 phần tử thì sẽ sắp xếp
#         arrayTemplate += [11] # thêm phần tử 11 vào mảng chính
#     arrayTemplate += arraySort[q] # thêm mảng con vào mảng chính
#     q += 1 # tăng biến đếm
#
# # Kiểm tra xem phần tử 11 còn xuất hiện duplicate ở chỗ nào trong mảng không
# s = 0
# while s < len(countNumber):
#     print(countNumber[s])
#     index = countNumber[s]
#     if arrayTemplate[index] != 11:
#         arrayTemplate.insert(index, 11)
#     s += 1
#
# print(arrayTemplate)
# # print(countNumber)
