# def ave(a, b, c):
#     return (a + b + c) / 3
#
#
# print(ave(1, 3, 2))
#
# ave = lambda a, b, c: (a + b + c) / 3
# print(ave(1, 3, 2))
#
# x_func = lambda a, b=1: a + b
# print(x_func(1, 4))
#
# def elcom():
#     intro = lambda x: x+' xin chao the gioi python \n'
#
#     intro2 = lambda y: y+' xin chao the gioi python'
#     return intro, intro2
#
# c1, c2 = elcom()
# print(c1('Nguyen Hoai Nam'), c2('Nguyen A'))


# kteam_lst = [lambda x: x**2, lambda x: x**3, lambda x: x**4] # một list với các phần tử là các hàm nặc danh
# print(kteam_lst[1](2))
# for func in kteam_lst:
#     print(func(3))


# find_greater = lambda x, y: x if x > y else y
# print(find_greater(1, 3))
# print(find_greater(6, 2))


def kteam(first_string):
    return lambda second_string: first_string + second_string # trả về một hàm, và lưu biến first_string

slogan = kteam('How Kteam ') # gửi giá trị cho biến first_string


print(slogan('Free Education')) # gửi nốt giá trị còn lại cho second_string

