# tools

# map
# ex1
# def inc(x): return x + 1;
#
#
# inc2 = lambda x: x + 1;
#
# elcom = [1, 2, 3, 4, 5];
#
# print(list(map(inc, elcom)))
#
# print(list(map(inc2, elcom)))
#
# print([inc2(x) for x in elcom])

# ex2

# tinhtong = lambda x, y: x + y
# elc1 = [1,2,3]
# elc2 = [6,7,8]
# a = map(tinhtong, elc1, elc2)
# print(list(a))


# filter()
# func = lambda x: x > 0
# k = [1, 4, 5, 6, 7, -7, 8, 0, 10]
# print(list(filter(func, k)))
# print(list(filter(None, k)))

# reduce

# reduce() lấy hai giá trị là index 0 và 1
from functools import reduce

elcom = [1, 2, 3, 4, 5]

elcom_add = lambda x, y: x + y
elcom_multil = lambda x, y: x * y
print(reduce(elcom_add, elcom, 10))
print(reduce(elcom_multil, elcom, 10))

