# Viết hàm  tao file trong folder project-python/100Example/ moi folder co 10 tap tin b21.py -> b10.py tu bai 21 -> 100
#
import os
from pathlib import Path

def create_files():
    base_dir = Path('100Example/')

    for i in range(41, 101):
        folder_num = (i - 1) // 10
        dir_path = base_dir / f'{folder_num * 10 + 1}_{folder_num * 10 + 10}'
        dir_path.mkdir(parents=True, exist_ok=True)

        file_path = dir_path / f'b{i}.py'
        file_path.touch()

create_files()
