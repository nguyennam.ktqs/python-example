# Bài 13 Kiểu dữ liệu List trong Python

# Kteam = [1, 5, 1, 6, 2, 7]
# print(Kteam.count(1))
# print(Kteam.count(2))

# Phương thức index
# Kteam = [1, 2, 3]
# print(Kteam.index(2))
# print(Kteam.index(4))

# <List>.copy(): Phương thức copy
# a = [1,2,3,4,5,6,7]
# b = a.copy()
# print(b)

# a = [1,2,3,4,5,6,7]
# b = a[:]
# print(b)

# Phương thức clear
# a = ['Xin chào']
# a.clear()
# print(a)

# a = ['Đây là thông tin mảng A']
# b = a
# print(b)
# b = []
# print(b)
# print(a)

# a = ['Đây là thông tin mảng A']
# b = a
# print(b)
# b.clear()
# print(b)
# print(a)

# phương thức append
a = [12,3,4,54,56,4,5,56]
a.append("them vao day nhé")
# Thêm một mảng vào
a.append([4,5,6])
a.append((1,2,3,4))
a.append(4.5)
a.append([[2,3],[4,5]])
print(a)

# Phương thức extend
# a = [1,2,3,4,[4,5,6]]
# a.extend([12,3,4,55])
# a.extend(['1','2',34,5])
# a.extend([[1,2,3,4],'45','66'])
# print(a)


# Phương thức insert
# a = [1,2,3,4]
# a.insert(1,5)
# a.insert(1,[6,7])
# a.insert(len(a), 20)
# a.insert(-1, 4)
# print(a)

# Phương thức pop
# kter= [1, 2, 3, 4, 5, 6]
# kter.pop(3)
# print(kter)
# kter.pop(-3)
# print(kter)
# kter.pop()
# print(kter)

# Phương thức remove
# kteam = [1, 5, 6, 2, 1, 7]
# kteam.remove(1)
# print(kteam)

# Phương thức reverse
# kteam = [1, 5, 6, 2, 1, 7]
# kteam.reverse()
# print(kteam)

# Phương thức sort
# howkteam= [3, 6, 7, 1, 2, 4]
# howkteam.sort()
# print(howkteam)

# lst = ['k', 'free', '9kteam', 'howkteam']
# lst.sort()
# print(lst)

# kteam = [6, 8, 2, 5, 1, 10, 4]
# true_reverse = kteam.copy() #tạo một bản sao của kteam và không ảnh hưởng đến kteam
# kteam.sort() # không đưa giá trị cho reverse thì mặc định là Fals
# true_reverse.sort(reverse=True)
# print(kteam)
# print(true_reverse)