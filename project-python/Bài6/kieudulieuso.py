# Bài 6: Kiểu dữ liệu số trong python
# a = 4
# print(a)
# print(type(a))
# print( 10 / 3)
# lấy toàn bộ nội dung của thư viện decimal
# from fractions import *
# print(Fraction(1, 4))
# print(Fraction(3, 9))
# lấy nội dung của thư viện math về sử dụng
# import math
# trả về phần nguyen cua mot so
# print(math.trunc(3.9))
# trả về một số thực là trị tuyệt đối
# print(math.fabs(-3))
# trả về căn thức bậc 2
# print(math.sqrt(16))
# trả về một số nguyên là ước chung của hai số truyền vào
# print(math.gcd(6, 4))

from decimal import Decimal, getcontext
# Đặt độ chính xác cho các phép tính Decimal là 30 chữ số
# getcontext().prec = 30
# Thực hiện các phép tính với Decimal
# result1 = Decimal(10) / Decimal(3)
# result2 = Decimal(100) / Decimal(3)
# Kiểm tra kiểu của số Decimal
# decimal_type = type(Decimal(5))
# In kết quả ra màn hình
# print(result1)  # 3.33333333333333333333333333333
# print(result2)  # 33.3333333333333333333333333333
# print(decimal_type)  # <class 'decimal.Decimal'>

# sopuhuc = complex(1, 2) # Dung ham complex de in ra dinh dang so phuc do ta dinh nghia
# print(sopuhuc)
# # Phan thuc
# print(sopuhuc.real)
# # Phan ao
# print(sopuhuc.imag)
# sp1 = type(3+1j)
# print(sp1)
