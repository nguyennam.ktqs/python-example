a = ['--'.join((a.capitalize(), b.upper() + c.lower())) for a, b, c in
     [('how', 'kteam', 'EDUCATION'), ('chia', 'sẻ', 'FREE')]]
# giải thích cho đoạn code này là nếu phần tử đầu tiên là 'how'
# và phần tử thứ 2 là 'kteam' và phần tử thứ 3 là 'EDUCATION' thì sẽ thực hiện thay đổi phần tử đầu tiên thành chữ in hoa
# và phần tử thứ 2 thành chữ in hoa và phần tử thứ 3 thành chữ in thường
# print(a)
#
# b = {key: value + 1 for key, value in (('Kteam', 69), ('Tèo', 50), ('Tũn', 14), ('Free Education', 93))
#      if value % 2 != 0}
# giải thích cho đoạn code này là nếu key là 'Kteam' thì sẽ thêm 1 vào value và nếu value không chia hết cho 2 thì sẽ thêm vào dict
# print(b)
#
student_list = ['Long', 'Trung', 'Giàu', 'Thành']
gen = enumerate(student_list, 8) # giải thích cho đoạn code này là sẽ thêm index vào phần tử trong list
# print(gen.__next__())
# print(gen.__next__())
# print(gen.__next__())
# print(gen.__next__())
# print(type(gen))
# for key, value in gen:
#     print(key, '=>', value)
#
# ex
# lst = [[1, 2, 3], [4, 5, 6]]
# for i in range(len(lst)):
#     print(i) # giải thích cho đoạn code này là sẽ in ra index của list
#     lst[i][0] = 'None' #
# print(lst)

# row = input('Nhập số hàng: ')
# col = input('Nhập số côt: ')
# print(row)
# print(col)
#
# x = [[a, b, c, d, e] for a, b, c, d, e in [range(5), range(5), range(5), range(5),
#                                                                         range(5)]]
#
# print(x)
