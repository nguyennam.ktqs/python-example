# main.py

# import a
# ex1
# a.say("Đây là file main")
# a.say("Chạy bởi hàm trong module a")


# ex2
# print(type(a))
#
# print(a.var_module_a)
# a.func_module_a()
# print(a.local_var)


# ex3
# from a import func_module_a
# func_module_a()

# ex4
# import a
# func_module_a = a.func_module_a
# del a # xóa biến a
# func_module_a()

# ex5
# from a import var_module_a, func_module_a
# #
# # print(var_module_a)
# # func_module_a()

# ex6
# import a
#
# print(a.var_module_a)
#
# a.var_module_a = 100
#
# import a
#
# print(a.var_module_a)

# ex7

# from a import var_module_a
#
# var_module_a = 100
#
# import a
#
# print(a.var_module_a)

# ex8
# import a
#
# a.var = 20
# print(a.var)
#
# import a
# print(a.var)
#
# from importlib import reload
#
# reload(a)
# print(a.var)