# Iteration là một khái niệm chung cho việc lấy từng phần tử một của một đối
# tượng nào đó, bất cứ khi nào bạn sử dụng vòng lặp hay kĩ thuật nào đó để có
# được giá trị một nhóm phần tử thì đó chính là Iteration.
# Ví dụ: như bạn ăn một snack, bạn sẽ lấy từng miếng trong bọc snack ra ăn
# cho tới khi hết thì thôi. Bạn có thể coi việc lấy bánh là một vòng lặp. Đương
# nhiên bạn cũng có thể chọn không lấy hết số bánh ra.

# Giới thiệu iterable object trong Python
# Iterable object là một object có phương thức __iter__ trả về một iterator, hoặc
# là một object có phương thức __getitem__ cho phép bạn lấy bất cứ phần tử
# nào của nó bằng indexing ví dụ như Chuỗi, List, Tuple.
# Các iterable object không nhất thiết phải là một list, tuple, set, dictionary.
#
# Iterator object đơn giản chỉ là một đối tượng mà cho phép ta lấy từng giá trị
# một của nó. Có nghĩa là bạn không thể lấy bất kì giá trị nào như ta hay làm với
# List hay Chuỗi.
# Iterator không có khả năng tái sử dụng trừ một số iterator có phương thức hỗ
# trợ như file object sẽ có phương thức seek.
# Iterator sử dụng hàm next để lấy từng giá trị một. Và sẽ có lỗi StopIteration
# khi bạn sử dụng hàm next lên đối tượng đó trong khi nó hết giá trị đưa ra cho
# bạn.
# Các iterable object chưa phải là iterator. Khi sử dụng hàm iter sẽ trả về một
# iterator. Đây cũng chính là cách các vòng lặp hoạt động.
# Ví dụ về iterable object

# a = [x for x in range(5)]
# print(iter(a)) #TypeError: 'list' object is not an iterato

# itor = (x for x in range(5))
# # print(itor) #itor là một iterator không sử dungj được hàm index
# print(next(itor))
# print(next(itor))
# print(next(itor))
# print(next(itor))
# print(next(itor))
# print(iter(itor)

# lst = [6, 3, 7, 'kteam', 3.9, [0, 2, 3]]
# iter_list = iter(lst) # iter_list là một iterator tạo từ list
# print(next(iter_list)) # 6
# print(next(iter_list)) # 6
# print(next(iter_list)) # 6
# print(next(iter_list)) # 6
# print(next(iter_list)) # 6
# print(next(iter_list)[-2]) # 6

# Một số hàm hỗ trợ cho iterable object trong Python
# 1 sum(iterable, start=0) # trả về tổng của các phần tử trong iterable
# print(sum([1, 6, 3]))  # 10
# print(sum([1, 6, 3], 10))
# print(sum(iter([1, 6, 3]), 10)) #giai thich cho vi du tren la vi du nay se tra ve 20 vi start = 10 + 1 + 6 + 3 = 20


# 2 min(iterable, *[, key, default]) # trả về phần tử nhỏ nhất trong iterable
# lấy ví dụ về hàm min
# print(min(iter([1, 6, 3])))  # 1
# print(min([],[1,2,3]))  # giải thích cho vi dụ này là nếu không có phần tử nào trong list thì sẽ trả về giá trị mặc định là 0
# dang trả ra rỗng vì không có phần tử nào trong list
# print(min([1, 6, 3], default=10)) # 1

# 3 max(iterable, *[, key, default]) # trả về phần tử lớn nhất trong iterable
# 4 all(iterable) # trả về True nếu tất cả phần tử trong iterable là True
# 5 any(iterable) # trả về True nếu có ít nhất một phần tử trong iterable là True
# 6 len(iterable) # trả về số lượng phần tử trong iterable
# 7 list(iterable) # trả về một list từ iterable
# 8 tuple(iterable) # trả về một tuple từ iterable
# 9 sorted(iterable, *[, key, reverse]) # trả về một list từ iterable được sắp xếp
# 10 reversed(iterable) # trả về một iterator từ iterable được đảo ngược
# 11 filter(function, iterable) # trả về một iterator chứa các phần tử trong iterable mà function trả về True
# 12 map(function, iterable, ...) # trả về một iterator chứa giá trị được tính từ function và iterable
# 13 zip(*iterables) # trả về một iterator chứa các tuple từ các iterable
# 14 enumerate(iterable, start=0) # trả về một iterator chứa các tuple index và value từ iterable
# 15 iter(object[, sentinel]) # trả về một iterator từ object
# 16 next(iterator[, default]) # trả về phần tử tiếp theo từ iterator
#lấy ví dụ với hàm seek trong iterator
# hàm seek chỉ dùng trong việc đọc file đúng không  ? không đúng
# seek dùng để di chuyển con trỏ đọc file đến vị trí nào đó trong file đó không nhất thiết phải là file text mà còn có thể là file nhị phân như file ảnh, file video, file âm thanh

