# Cách khai báo chuỗi
# 1-Cách dùng dấu nháy đơn hoặc dấu nháy kép
# str1 = "I'm Beginner"
# str2 = 'I"m Beginner'
# print(str1)
# print(str2)
#  2-Khi có nhiều dấu nháy đơn hoặc kép trong chuỗi
# str3 = "I'm a 'Beginner'"
# str4 = 'I"m a "Beginner"'
# print(str3)
# print(str4)
# 3-Trong chuỗi có chứa dấu nháy đơn hoặc kép
# str5 = 'I\'m a "Beginner"'
# print(str5)
# 4-Liêt kê escape sequences
# \n: Newline
# \t: Tab
# \\: Backslash
# \': Single quote
# \": Double quote
# \b: Backspace
# \r: Carriage return
# \f: Form feed
# \v: Vertical tab
# \ooo: Character with octal value ooo
# \xhh: Character with hex value hh
print("This is a newline character: \\n")
print("This is a tab character: \\t")
print("This is a backslash: \\\\")
print("This is a single quote: \\'")
print("This is a double quote: \\\"")
print("This is a backspace: \\b")
print("This is a carriage return: \\r")
print("This is a form feed: \\f")
print("This is a vertical tab: \\v")
print("This is a character with octal value ooo: \\ooo")
print("This is a character with hex value hh: \\xhh")
