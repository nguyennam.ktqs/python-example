    # Bài 14 Kiểu dữ liệu tuple

# Giới thiệu về Tuple trong Python
# a = (1, 2, 3, 4, 5)
# print(a)
# b = ('k', 't', 'e', 'r') # Một Tuple chứa 4 chuỗi
# print(b)
# c = ([1, 2], (3, 4)) # Một Tuple chứa 1 List là [1, 2] và 1 Tuple là (3, 4)
# print(c)
# q = (1, 'kteam', [2, 'k9']) # Tuple chứa số nguyên, chuỗi, và List
# print(q)

# Cách khởi tạo Tuple
# Một số toán tử với Tuple trong Python

# Toán tử +
# tup = [1, 2]
# tup += ('how', 'kteam')
# print(tup)

# Toán tử *
# tup = tuple('kter') * 3
# print(tup)

# Toán tử in
# w = -2 in (1, 2, 3)
# print(w)

# Indexing và cắt Tuple trong Python
# tup = (1, 2, 'a', 'b', [3, 4])
# print(len(tup))
# print(tup[0])
# print(tup[-1])
# print(tup[3])
# print(tup[1:3])
# print(tup[:2])
# print(tup[2:])
# print(tup[::-1])

# Tuple có phải luôn luôn là một hash object?

# tup = ([1, 2],)
# tup[0][0] = 'howkteam'
# print(tup)

# Các phương thức của Tuple
tup = (1, 5, 3, 5, 6, 1, 1)
# <Tuple>.count(value)
# print(tup.count(1))
# print(tup.index(6))
