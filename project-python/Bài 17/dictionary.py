# Bài 17 Dictionary
# dict = {'name': 'Kteam', 'member': 69}
# print(dict)

# dict = {}
# print(dict)
# print(type(dict))

# dic = {key: value for key, value in [('name', 'Kteam'), ('member', 69)]}
# print(dic)

# a1 = dict()
# print(a1)

# class Map_Class:
#    def keys(self):
#          return [1, 2, 3]
#    def __getitem__(self, key):
#          return key * 5
#
# map_obj = Map_Class()
# dic = dict(map_obj)
# print(dic)


# iter_ = [('name', 'Kteam'), ('member', 69)]
# dict = dict(iter_)
# print(dict)
#
# iter_1 = [['name', 'Kteam'], {'member', 69}]
# dict1= dict(iter_1)
# print(dict1)

# iter_ = (['name', 'Kteam'], ['member', 69])
# dict = dict(iter_)
# print(dict)


# dict = dict(name1='name', member1='Kteam')
# print(dict)
#
# iter_ = ('name', 'number') # tuple
# dic_none = dict.fromkeys(iter_)
# print(dic_none)
# dic = dict.fromkeys(iter_, 'non None value')
# print(dic)

# a = {'name': 'Kteam', 'member': 69}
# print(a)
# print(a['name'])
# a['name'] = 'Nam'
# print(a)
# a['name'] += 'Nam Nguyen'
# print(a)
# a['age'] = 30
# print(a)
# a['age'] = 30 +30
# print(a)
