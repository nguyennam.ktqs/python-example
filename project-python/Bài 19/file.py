# Bài 19 file trong Python

# a = help()



# r  => mở để đọc
# r+ => mở để đọc và ghi
# w => mở để ghi, trước đó sẽ xóa hết nội dung của file, nếu chưa có file sẽ tạo mới file
# w+ => mở để đọc và ghi, trước đó sẽ xóa hết nội dung của file, nếu chưa có file sẽ tạo mới file
# a => mở để ghi, nếu chưa có file sẽ tạo mới file
# a+ => mở để đọc và ghi, nếu chưa có file sẽ tạo mới file

file_object = open('./nam.txt')
# print(file_object)
# print(type(file_object))
# a = file_object.read(5)
# print(a)
# b = file_object.read(10)
# print(b)
# file_object.close()


# fobj = open('./nam.txt')
# data1 = fobj.readline()
# data = fobj.readlines()
# fobj.close()
# print(data1)
# print(data)

# file = open('./nam.txt')
# list_content = list(file)
# file.close()
# print(list_content)
# file = open('./nam.txt')
# list_tuple = tuple(file)
# file.close()
# print(list_tuple)

# Tại sao không in ra gì cả
# 1 file chỉ đọc được 1 lần, nếu đọc 1 lần rồi thì không thể đọc lại được

# Nếu không đóng file thì file sẽ bị mở mãi mãi
# Mở mãi mãi thì sao?
# Nếu mở mãi mãi thì sẽ tốn tài nguyên máy tính, và khi mở file khác sẽ không thể mở được

# fobj = open('./nam.txt')
# data = fobj.read()
# print(data)

# r  => mở để đọc
# r+ => mở để đọc và ghi # nếu chưa có file sẽ báo lỗi
# w => mở để ghi, trước đó sẽ xóa hết nội dung của file, nếu chưa có file sẽ tạo mới file
# w+ => mở để đọc và ghi, trước đó sẽ xóa hết nội dung của file, nếu chưa có file sẽ tạo mới file
# a => mở để ghi, nếu chưa có file sẽ tạo mới file
# a+ => mở để đọc và ghi, nếu chưa có file sẽ tạo mới file
# fobj = open('./nam.txt', 'a+') # mở file để ghi
# result = fobj.write('Chao mung ban den voi the gioi Python \n') # trả về số kí tự đã ghi
# print(result)
# fobj.close()

# fobj = open('./nam.txt')
# fobj.seek(10) # di chuyển con trỏ đọc file đến vị trí 10
# data2 = fobj.read()
# fobj.close()
# print(data2)

# with open('./nam.txt') as fobj:
#     data = fobj.read()
#     fobj.close()
#     print(data)
