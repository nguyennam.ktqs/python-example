# Bài 9: Kiểu dữ liệu chuỗi phần 3
# %s truyền vào một chuỗi
# %r truyền vào một đối tượng
# %d truyền vào một số

# 9.1 Định dạng bằng toán tử %
# a = 'Xin chào %s.' %('Nguyen Hoai Nam')
# print(a)

# b = "Chúc mừng sinh nhật %s %d Năm" %('Elcom',25.5)
# print(b)

# c = '%s %s' %('one','two')
# print(c)

# b = '%s %s'
# # d = b %('ELCOM',30)
# # print(d)

# class SomeThing:
#     def __repr__(self):
#         return 'Đây là __repr__'
#     def __str__(self):
#         return 'Đây là __str__'
#
# sthing = SomeThing()
# print(sthing)
# print(type(sthing))
#
# a = '%r' %(sthing)
# print(a)
# b = '%s' %(sthing)
# print(b)

# $s và %r đều dùng để thể hiện của kiểu dữ liệu str, int, float, list, tuple

# s = '%s' %(1)
# r = '%r' %(1)
# print(s)
# print(r)

# Kiểu mảng
# s = '%s' %([1,2,3])
# r = '%r' %([1,2,3])
# print(s)
# print(r)

# 9.2 Định dạng bằng chuỗi f

#ex1:
# variable = "Python"
# q = 'Chào Mừng bạn đến với thế giới của {variable}'
# q1 = f'Chào Mừng bạn đến với thế giới của {variable}.\n Chỗ này xuống dòng nhé \t chỗ này dùng tab nhé'
# print(q)
# print(q1)

#ex2
# b = '3'
# q2 = f'1:{{2}},2:{{2}},3:{b} em có đánh rơi nhịp nào không ^^'
# print(q2)
# ==> cặp dấu {{}} triệt tiêu cặp dấu {} thể hiện biến

# 9.3 Định dạng bằng phương thức format

#ex1
# a = 'a:{}, b:{}, c:{}'.format(1,2,3)
# print(a)
# b = 'a:%d, b:%d, c:%d' %(1,2,3)
# print(b)
# c = 'a:%r, b:%s, c:%s' %([1,2,3],2,3)
# print(c)

# ex2
# a = 'nhận giá trị {0} của hàm format(1,2)'.format(1,2)
# print(a)
# a = 'nhận giá trị {1} của hàm format(1,2)'.format(1,2)
# print(a)
# a = 'nhận 2 giá trị {0} {0} của hàm format(1,2)'.format(1,2)
# print(a)

#Căn lề
# a = '{:^50}'.format('Xin chào thế giới')  # căn giữa
# print(a)
# b = '{:<50}'.format('Xin chào thế giới') # căn lề phải
# print(b)
# c = '{:>50}'.format('Xin chào thế giới') # căn lề trái
# print(c)
# d = '{:*>10}'.format('aaaa') # căn lề trái, thay thế khoảng trắng bằng kí tự *
# print(d)
# e = '{:*<10}'.format('aaaa') # căn lề trái, thay thế khoảng trắng bằng kí tự *
# print(e)
# f = '{:*^10}'.format('aaaa') # căn giữa, thay thế khoảng trắng bằng kí tự *
# print(f)
#
# row_1 = '+ {:-<6} + {:-^15} + {:->10} +'.format('', '', '')
# row_2 = '| {:<6} | {:^15} | {:>10} |'.format('ID', 'Ho va ten', 'Noi sinh')
# row_3 = '| {:<6} | {:^15} | {:>10} |'.format('123', 'Yui Hatano', 'Japanese')
# row_4 = '| {:<6} | {:^15} | {:>10} |'.format('6969', 'Sunny Leone', 'Canada')
# row_5 = '+ {:-<6} + {:-^15} + {:->10} +'.format('', '', '')
#
# phần xuất kết quả
# print(row_1)
# print(row_2)
# print(row_3)
# print(row_4)
# print(row_5)
