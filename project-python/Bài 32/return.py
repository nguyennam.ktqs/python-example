# Bai1
# def cal_rec_per(width, height):
#     per = (width + height) * 2
#     return per
# rec_1_width = 5
# rec_1_height = 3
# khởi tạo một biến để hứng kết quả
# rec_1_per = cal_rec_per(rec_1_width, rec_1_height)
# print(rec_1_per)
# trường hợp này là khi bạn không cần tái sử dụng nó ở lần sau
# print(cal_rec_per(7, 4))

# Bai2
# def _return_ter_func():
#     print('chúng ta sử dụng return để ngắt hàm')
#     dòng dưới đây tương tự như bạn viết return None
# return
# print('Hàm print này dĩ nhiên không được gọi')
# none = _return_ter_func()
# print(type(none))

# Bai3
# def cal_rec_area_per(width, height):
#     perimeter = (width + height) * 2
#     area = width * height
#     return perimeter, area, 999
#
# rec_width = 3
# rec_height = 9
# rec_per, rec_area, a = cal_rec_area_per(rec_width, rec_height)
# print(rec_per, rec_area, a)

# Bai4

# Cách 1
# pointer = [(-5, -20), (-4, -15), (-3, 4), (-2, 9), (-1, 7), (0, 1), (1, -7), (2, -9), (4, 81), (5, 130)]
# def checkPointer(aPointer):
#     aPointerPass = 0
#     aPointerNoPass = 0
#     for value in aPointer:
#         # Tính ra tọa độ của điểm y
#         y = calculator(value[0])
#         # print(f'Tọa độ điểm {value}')
#         if y == value[1]:
#             # print(f' Đi qua y = {y}')
#             aPointerPass += value[1]
#             # print(f'=============aPointerPass = {aPointerPass}')
#         else:
#             # print(f'Không đi qua y = {y}')
#             aPointerNoPass += value[1]
#             # print(f'=============aPointerNoPass = {aPointerNoPass}')
#     b = aPointerPass - aPointerNoPass
#     return abs(b)
#
# def calculator(x):
#     result2 = (x**3) + (2*(x**2)) - (4*x) +1
#     return result2
#
# print('trị tuyệt đối của hiệu tổng tung độ hai list')
# a = checkPointer(pointer)
# print(a)

# Cách 2
# =====================================
# def f_x(x):
#     return x**3 + (2 * (x**2)) - (4 * x) + 1
#
# def check_point(x, y):
#     if y == f_x(x):
#         return True
#     return False
#
# def fil_point(lst_point):
#     lst_A, lst_B = [], []
#     for l in lst_point:
#         if check_point(*l):
#             lst_A.append(l)
#             continue
#         lst_B.append(l)
#     return lst_A, lst_B
#
# def cal_sum(lst):
#     s = 0
#     for value in lst:
#         s += value[1]
#     return s
#
# lst = [(-5, -20), (-4, -15), (-3, 4), (-2, 9), (-1, 7), (0, 1), (1, -7), (2, -9), (4, 81), (5, 130)]
# lst_A_after, lst_B_after = fil_point(lst)
# print(abs(cal_sum(lst_A_after) - cal_sum(lst_B_after)))

# Làm lại

# lstPointer = [(-5, -20), (-4, -15), (-3, 4), (-2, 9), (-1, 7), (0, 1), (1, -7), (2, -9), (4, 81), (5, 130)]
# # listA = []
# # listB = []
# # def filPointer(lstPointer):
# #     for value in lstPointer:
# #         x = value[0]
# #         y = x ** 3 + (2 * (x ** 2)) - (4 * x) + 1;
# #         if y == value[1]:
# #             listA.append(value)
# #         else:
# #             listB.append(value)
# #         x = y = 0
# #     print(listA)
# #     print(listB)
# #
# # def cal_sum(lst):
# #     s = 0
# #     for value in lst:
# #         s += value[1]
# #     return s
# # filPointer(lstPointer)
# # print(abs(cal_sum(listA) - cal_sum(listB)))