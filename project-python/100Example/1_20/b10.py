# Bài 10:
# Câu hỏi:
# Viết một chương trình có 2 chữ số, X, Y nhận giá trị từ đầu vào và tạo ra một mảng 2
# chiều. Giá trị phần tử trong hàng thứ i và cột thứ j của mảng phải là i*j.
# Lưu ý: i=0,1,...,X-1; j=0,1,...,Y-1.
# Ví dụ: Giá trị X, Y nhập vào là 3,5 thì đầu ra là: [[0, 0, 0, 0, 0], [0, 1, 2, 3, 4], [0, 2, 4,
# 6, 8]]
# Gợi ý:
#  Viết lệnh để nhận giá trị X, Y từ giao diện điều khiển do người dùng nhập vào.


input_str = input("Nhập X, Y: ")
# Dùng Comprhension tạo mảng cho hai số X,Y nhập vào
dimensions = [int(x) for x in input_str.split(',')]
rowNum = dimensions[0]
colNum = dimensions[1]
print(dimensions)
print(rowNum)
print(colNum)
# Dựng khung cho mảng nhiều chiều
multilist = [[0 for col in range(colNum)] for row in range(rowNum)]
# For lồng nhau để insert phần tử của mảng vào từng index
for row in range(rowNum):
    for col in range(colNum):
        multilist[row][col] = row * col
print(multilist)
