# Bài 08:
# Câu hỏi:
# Định nghĩa một lớp gồm có tham số lớp và có cùng tham số instance
# Gợi ý:
#  Khi định nghĩa tham số instance, cần thêm nó vào __init__
#  Bạn có thể khởi tạo một đối tượng với tham số bắt đầu hoặc thiết lập giá trị sau
# đó.

class Person:
    name = "Person"

    def __init__(self, name=None):
        self.name = name

a = Person('Nguyen Hoai Nam')
print('%s is name %s' % (Person.name, a.name))
