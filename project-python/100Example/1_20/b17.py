# Bài 17:
# # # Câu hỏi:
# # #
# # # Tài liệu được chia sẻ tại: http://nhasachtinhoc.blogspot.com
# # # Viết một chương trình chấp nhận đầu vào là một câu, đếm chữ hoa, chữ thường.
# # # Giả sử đầu vào là: Quản Trị Mạng
# # # Thì đầu ra là:
# # # Chữ hoa: 3
# # # Chữ thường: 8
# # # Gợi ý:
# # # Trong trường hợp dữ liệu đầu vào được nhập vào chương trình nó nên được giả
# # # định là dữ liệu được người dùng nhập vào từ giao diện điều khiển.

input = input('Nhập ký tự cần tính:')
d = {'UPPER_CASE':0,'LOWER_CASE':0}
for c in input:
    if c.islower():
        d['LOWER_CASE'] += 1
    elif c.isupper():
        d['UPPER_CASE'] += 1
    else:
        pass
print('Chữ thường:', d['LOWER_CASE'])
print('Chữ hoa:', d['UPPER_CASE'])