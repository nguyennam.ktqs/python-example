# Bài 12:
# Câu hỏi:
#
# Tài liệu được chia sẻ tại: http://nhasachtinhoc.blogspot.com
# Viết một chương trình chấp nhận chuỗi là các dòng được nhập vào, chuyển các
# dòng này thành chữ in hoa và in ra màn hình. Giả sử đầu vào là:
# Hello world
# Practice makes perfect
# Thì đầu ra sẽ là:
# HELLO WORLD
# PRACTICE MAKES PERFECT
# Gợi ý:
# Trong trường hợp dữ liệu đầu vào được nhập vào chương trình nó nên được giả
# định là dữ liệu được người dùng nhập vào từ giao diện điều khiển.

lines = []
# Dùng vòng lặp while chạy đến khi nào xuất hiện dấu cách thì sẽ in ra những input vừa nhập vào
while True:
    s = input('Nhập dữ liệu đầu vào:')
    if s:
        lines.append(s.upper())
    else:
        break;
# Bài Python 12, Code by Quantrimang.com
print(lines)
for sentence in lines:
    print(sentence)
