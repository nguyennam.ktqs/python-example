# Bài 19:
# Câu hỏi:
# Sử dụng một danh sách để lọc các số lẻ từ danh sách được người dùng nhập vào.
# Giả sử đầu vào là: 1,2,3,4,5,6,7,8,9 thì đầu ra phải là: 1,3,5,7,9
# Gợi ý:
# Trong trường hợp dữ liệu đầu vào được nhập vào chương trình nó nên được giả
# định là dữ liệu được người dùng nhập vào từ giao diện điều khiển.

# Cách 1:
# input = input('Nhập dữ liệu đầu vào:').split(',')
# le = []
# chan = []
# for item in input:
#     s = int(item)
#     if (s % 2 != 0):
#         le.append(s)
#     else:
#         chan.append(s)
#
# print(le)
# print(chan)

# Cách 2:
# value = input("Nhập dữ liệu đầu vào").split(',')
# number = [x for x in value if int(x) %2 != 0]
# print(','.join(number))