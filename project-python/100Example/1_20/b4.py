# Bài 04:
# Câu hỏi:
# Viết chương trình chấp nhận một chuỗi số, phân tách bằng dấu phẩy từ giao diện
# điều khiển, tạo ra một danh sách và một tuple chứa mọi số.
# Ví dụ: Đầu vào được cung cấp là 34,67,55,33,12,98 thì đầu ra là:
# ['34', '67', '55', '33', '12', '98']
# ('34', '67', '55', '33', '12', '98')
# Gợi ý:
#  Viết lệnh yêu cầu nhập vào các giá trị sau đó dùng quy tắc chuyển đổi kiểu dữ
# liệu để hoàn tất.

value = input("Nhập vào các giá trị:")
l = value.split(",")
t = tuple(l)
z = dict(l)
# giai thich {'3': '3', '6': '7', '5': '5', '1': '2', '9': '8'} ket qua mong muon {'3': 4, '6': 7, '5': 5, '1': 2, '9': 8}
# tai sao phan tu sau tien no lai la: 3': '3' ma khong phai la 3:4
# vi du: 3:4 la 3 la key va 4 la value
print(l)
print(t)
print(z)
