# Bài 02:
# # Câu hỏi:
# # Viết một chương trình có thể tính giai thừa của một số cho trước. Kết quả được in
# # thành chuỗi trên một dòng, phân tách bởi dấu phẩy. Ví dụ, số cho trước là 8 thì kết
# # quả đầu ra phải là 40320.
# Định nghĩa giai thừa: https://vi.wikipedia.org/wiki/Giai_th%E1%BB%ABa
# Đệ quy trong Python là một hàm gọi chính nó.
# Đệ quy là một cách mà một hàm gọi chính nó.
# Đệ quy thường được sử dụng để giải quyết các vấn đề mà có thể được chia nhỏ thành các vấn đề nhỏ hơn tương tự.
# Đệ quy trong Python hoạt động như sau: khi một hàm gọi chính nó,
# hàm gọi sẽ được tạm dừng và một phiên bản mới của hàm sẽ được tạo ra để xử lý vấn đề.
# Điều này tiếp tục cho đến khi vấn đề được giải quyết và kết quả được trả về, sau đó các cuộc gọi hàm trước đó tiếp tục thực thi.
# Đệ quy giúp giảm độ phức tạp của vấn đề và giúp chương trình dễ đọc hơn.
# Đệ quy trong Python có thể được sử dụng để giải quyết nhiều vấn đề như tìm kiếm, sắp xếp, v.v.

x = int(input('Nhập số cần tính giai thừa: '))
print(x)


def fact(x):
    if x == 0:
        return 1
    else:
        return x * fact(x - 1)


print(fact(x))
