# Câu hỏi:
# Định nghĩa một class có ít nhất 2 method:
#     getString: để nhận một chuỗi do người dùng nhập vào từ giao diện điều khiển.
# printString: in chuỗi vừa nhập sang chữ hoa.
# Thêm vào các hàm hiểm tra đơn giản để kiểm tra method của class.
#     Ví dụ: Chuỗi nhập vào là quantrimang.com thì đầu ra phải là: QUANTRIMANG.COM
# Gợi ý:
#  Sử dụng __init__ để xây dựng các tham số.
class inputOutString(object):
    def __init__(self): # constructor
        self.s = ""

    def getString(self):
        self.s = input("Nhập chuỗi:")

    def printString(self):
        print(self.s.upper())


strObj = inputOutString()
strObj.getString()
strObj.printString()

