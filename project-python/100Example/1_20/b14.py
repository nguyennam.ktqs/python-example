# Bài 14:
# Câu hỏi:
# Viết một chương trình chấp nhận đầu vào là chuỗi các số nhị phân 4 chữ số, phân
# tách bởi dấu phẩy, kiểm tra xem chúng có chia hết cho 5 không. Sau đó in các số
# chia hết cho 5 thành dãy phân tách bởi dấu phẩy.
# Ví dụ đầu vào là: 0100,0011,1010,1001
# Đầu ra sẽ là: 1010
# Gợi ý:
# Trong trường hợp dữ liệu đầu vào được nhập vào chương trình nó nên được giả
# định là dữ liệu được người dùng nhập vào từ giao diện điều khiển.
value = []
items = [x for x in input("Nhập các số nhị phân: ").split(',')]
for p in items:
    intp = int(p, 2)  # chuyển số nhị phân sang số thập phân
    if (intp % 5 == 0):  # nếu intp chia hết cho 5 1010 là 10 1001 là 9 lam sao chia het cho 5 được ?
        value.append(p)
# Bài tập Python 14, Code by Quantrimang.com
print(','.join(value))
