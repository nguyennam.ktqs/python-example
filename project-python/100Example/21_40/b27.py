# Bài 27:
# Câu hỏi:
# Định nghĩa một hàm có thể chuyển số nguyên thành chuỗi và in nó ra giao diện điều
# khiển
# Gợi ý:
# Sử dụng str() để chuyển đổi một số thành chuỗi.
# Code mẫu:
def printValue(n):
    print(str(n))
    print(type(n))

printValue(10)
