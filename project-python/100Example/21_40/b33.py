# Bài 33:
# Câu hỏi:
# Định nghĩa một hàm có thể in dictionary chứa các key là số từ 1 đến 20 (bao gồm cả
# 1 và 20) và các giá trị bình phương của chúng.
# Gợi ý:
#  Sử dụng dict[key]=value để nhập mục vào dictionary.
#  Sử dụng toán từ ** để lấy bình phương của một số.
#  Sử dujnng range() cho các vòng lặp.
# Code mẫu:
# def printDict():
#     d=dict()
#     for i in range(1,21):
#         d[i]=i**2
#     print (d)
# printDict()

# a = 1 + 2 +3 + ... + 100 viết bằng python
# sum = 0
# for i in range(1, 101):
#     sum += i
# print(sum)
# Tim uoc cua 100
for i in range(1, 101):
    if 100 % i == 0:
        print(i)

# tim boi của 2 trong khoảng 1 đến 100
for i in range(1, 101):
    if i % 3 == 0:
        print(i)
