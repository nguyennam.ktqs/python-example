# Bài 29:
# Câu hỏi:
# Tài liệu được chia sẻ tại: http://nhasachtinhoc.blogspot.com
# Định nghĩa hàm có thể nhận 2 chuỗi từ input và nối chúng sau đó in ra giao diện điều
# khiển
# Gợi ý:
# Sử dụng + để nối các chuỗi.

def noiChuoi(s1, s2):
    a = s1 + ' ' + s2
    return a


print(noiChuoi('Hello', 'World'))
