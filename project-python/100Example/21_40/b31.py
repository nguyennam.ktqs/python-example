# Bài 31:
# Câu hỏi:
# Định nghĩa hàm có thể chấp nhận input là số nguyên và in "Đây là một số chẵn" nếu
# nó chẵn và in "Đây là một số lẻ" nếu là số lẻ.
# Gợi ý:
# Sử dụng toán tử % để kiểm tra xem số đó chẵn hay lẻ

def checkNumber(n):
    if n % 2 == 0:
        print(str(n) + ' Đây là một số chẵn')
    else:
        print(str(n) + ' Đây là một số lẻ')


checkNumber(10)
