# Bài 30:
# Câu hỏi:
# Định nghĩa một hàm có input là 2 chuỗi và in chuỗi có độ dài lớn hơn trong giao diện
# điều khiển. Nếu 2 chuỗi có chiều dài như nhau thì in tất cả các chuỗi theo dòng.
# Gợi ý:
# Sử dụng hàm len() để lấy chiều dài của một chuỗi
s1 = input('Nhập chuỗi 1: ')
s2 = input('Nhập chuỗi 2: ')
if len(s1) > len(s2):
    print(s1)
else:
    if len(s1) < len(s2):
        print(s2)
    else:
        print(s1)
        print(s2)
