# Bài 22:
# # Câu hỏi:
# # Viết chương trình sắp xếp tuple (name, age, score) theo thứ tự tăng dần, name là
# # string, age và height là number. Tuple được nhập vào bởi người dùng. Tiêu chí sắp
# # xếp là:
# # Sắp xếp theo name sau đó sắp xếp theo age, sau đó sắp xếp theo score. Ưu tiên là
# # tên > tuổi > điểm.
# # Nếu đầu vào là:
# # Tom,19,80
# # John,20,90
# # Jony,17,91
# # Jony,17,93
# # Json,21,85
# #
# # Tài liệu được chia sẻ tại: http://nhasachtinhoc.blogspot.com
# #
# # Thì đầu ra sẽ là:
# # [('John', '20', '90'), ('Jony', '17', '91'), ('Jony', '17', '93'), ('Json', '21', '85'), ('Tom', '19',
# # '80')]
# # Gợi ý:
# # Trong trường hợp dữ liệu đầu vào được nhập vào chương trình nó nên được giả
# # định là dữ liệu được người dùng nhập vào từ giao diện điều khiển.
# # Sử dụng itemgetter để chấp nhận nhiều key sắp xếp.



# Bài tập Python 22 Code by Quantrimang.com
# Tom,19,80
# John,20,90
# Jony,17,91
# Jony,17,93
# Json,21,85
from operator import itemgetter, attrgetter
l = []
print('Nhập dữ liệu đầu vào: ')
while True:
    s = input()
    if not s: # nếu không có dữ liệu thì thoát
        break
    l.append(tuple(s.split(",")))
print(sorted(l, key=itemgetter(1, 0, 2)))
# sorted(l, key=itemgetter(1, 0, 2)) sắp xếp theo age, name, score

