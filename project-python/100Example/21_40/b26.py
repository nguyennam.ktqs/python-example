# Bài 26:
# Câu hỏi:
# Định nghĩa 1 hàm có thể tính tổng hai số.
# Gợi ý:
# Định nghĩa 1 hàm với 2 số là đối số. Bạn có thể tính tổng trong hàm và trả về giá trị
def sum(a, b):
    return a + b
print(sum(1, 2))
