# Bài 11: Kiểu dữ liệu chuỗi phần 5

# 11.1 Các phương thức tách chuỗi
# <chuỗi>.split(sep=None, maxsplit=-1)
# a = 'Chào mừng bạn đến với thế giới của Python'
# print(a.split())
# print(a.split('m',maxsplit=2))

# q = 'How--Kteam--K9'.split('--')
# print(q)
#
# w = 'How kteam EDUCATION'.rsplit();
# print(w)
#
# e = 'How kteam EDUCATION'.rsplit(maxsplit=1);
# print(e)
# s = "apple banana cherry"

# Using split()
# split_list = s.split(' ', 1)
# print(split_list)  # Output: ['apple', 'banana cherry']

# Using rsplit()
# rsplit_list = s.rsplit(' ', 1)
# print(rsplit_list)  # Output: ['apple banana', 'cherry']

# <chuỗi>.partition(sep) => Trả về một tuple với 3 phần tử. Các phần tử đó lần lượt là chuỗi trước chuỗi sep, sep và  chuỗi sau sep.
# r = 'How kteam vs I hate python team vs Education'.partition('vs')
# print(r)
# t = 'How kteam vs I hate python team vs Education'.partition('VS')
# print(t)

# <chuỗi>.rpartition(sep) =>Cách phân chia giống như phương thức partition nhưng lại chia từ phải qua trái. Và với sep không có trong chuỗi thì sẽ trả về 2 giá trị đầu tiên là chuỗi rỗng và cuối cùng là chuỗi ban đầu

# 11.2 Các phương thức tiện ích
# <chuỗi>.count(sub, [start, [end]]) => Trả về một số nguyên, chính là số lần xuất hiện của sub trong chuỗi. Còn start và end là số kĩ thuật slicing (lưu ý không hề có bước).
# y = 'kkkkk'.count('k')
# print(y)
# u = 'kkkkk'.count('kk')
# print(u)
# i = 'kkkkk'.count('k', 3)
# print(i)
# o = 'kkkkk'.count('kk', 3, 4)
# print(o)

# <chuỗi>.startswith(prefix[, start[, end]]) =>Trả về  giá trị True nếu chuỗi đó bắt đầu bằng chuỗi prefix. Ngược lại là False.
# p = 'how kteam free education'.startswith('ho')
# print(p)
# a = 'how kteam free education'.startswith('ho', 4)
# print(a)
# s = 'how kteam free education'.startswith('ho', 0)
# print(s)

# <chuỗi>.endswith(prefix[, start[, end]]) =>Trả về  giá trị True nếu chuỗi đó kết thúc bằng chuỗi prefix. Ngược lại là Flase.
# d = 'how kteam free education'.endswith('n')
# print(d)
# f =  'how kteam free education'.endswith('ho')
# print(f)
# g = 'how kteam free education'.endswith('n', 0, 24)
# print(g)

# <chuỗi>.find(sub[, start[, end]]) => Trả về một số nguyên, là vị trí đầu tiên của sub khi dò từ trái sang phải trong chuỗi. Nếu sub không có trong chuỗi, kết quả sẽ là -1. Vẫn như các phương thức khác, start end đại diện cho slicing và ta sẽ tìm trong chuỗi slicing này.


# h = 'howkteam'.find('h')
# print(h)
# j = 'howkteam'.find('h', 2)
# print(j)
# k = 'howkteamhow'.rfind('h') # Tương tự phương thức find nhưng tìm từ phải sang trái
# print(k)
# l = 'abcd'.index('b')
# print(l)



# 11.3 Các phương thức xác thực
# z = 'python'.islower() # Trả về True nếu tất cả các kí tự trong chuỗi đều là viết thường. Ngược lại là False
# print(z)

# x = 'HOWKTEAM'.isupper()
# print(x)
#
# c = 'Free Education'.istitle() #Các chữ cái đầu ký tự đều viết hoa
# print(c)

# v = '0123'.isdigit() #Trả về True nếu tất cả các kí tự trong chuỗi đều là những con số từ 0 đến 9
# print(v)
#
# b = '      '.isspace()
# print(b)
#
# n = '   d  '.isspace()
# print(n)
# bài tập  về nhà
# s = 'aaaAAaaaooaaneu mot Ngay naO Doaaaaaaa'
# a = s.partition('neu mot Ngay naO Do')
# print(a[1].title())
# b = s.lower()
# print(b)
# c = b.strip('a')
# d = c.lstrip('oa')
# print(d.title())
