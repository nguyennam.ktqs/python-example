# Bài 8
# 8.1.Chuỗi trần là gì?
# print('Haizz, \neu mot ngay nao do') #Ah, bây giờ tôi đã hiểu vấn đề của bạn. Lỗi xảy ra khi có một khoảng trắng trước chữ print ở dòng đầu tiên, khiến Python nhận diện mã lệnh không đúng cấu trúc. Python là ngôn ngữ lập trình nhạy cảm với khoảng trắng (whitespace sensitive), có nghĩa là thụt lề (indentation) phải đúng để xác định các khối mã.
# print('Haizz, \neu mot ngay nao do')

# print('Haizz, \\neu mot ngay nao do')
# a = r'\neu mot ngay'
# print(a)

# 8.2.Một số toán tử với chuỗi
# s: str = 'hello'
# s = 1 : so thi khong cong duoc voi chuoi
# s += ' Python'
# print(s)

# Toán tử *
# 8.2.1.tạo ra một chuỗi nhờ lặp đi lặp lại chuỗi với số lần bạn muốn.
# a = 'a\n' * 3
# print(a)

# 8.2.2.cách viết khác khi lặp lại toán tử
# a = 'a'
# a *= 100
# print(a)

# 8.2.3.Bất cứ một chuối nào nhân với 0 | số ấm đều trả ra một chuỗi rỗng
# a = 'hoc python'
# a = a * 0
# a = a * -2
# a = '8523nsalfnsdf' * -2
# print(a)

# 8.2.4 Toán tử in
# a = 'a' in 'abc'
# print(a)
# a = 'ac' in 'abc'
# print(a)

# 8.3 Indexing và cắt chuỗi
# 8.3.1 Indexing
# s = 'abc xyz'
# print(s)
# print(s[0])
# print(s[2])
# print(s[6])
# print(s[-1])
# print(s[-2])
# <chuỗi>[vị trí bắt đầu : vị trí dừng]
q = 'Đây là khóa học cơ bản python'
# z = q[0:9]
# print(z)

# 8.3.2 Cắt chuỗi
# w = q[-4:-1] #tho
# print(w)
# r = q[4: -1]  # cắt từng kí tự có vị trí từ 1(-6) đến 5(-2) vì vị trí dừng là 6(-1)
# print(r) #là khóa học cơ bản pytho
# t = q[:2] + q[2:3]
# print(t)
# y = len(t)
# print(y)
# 3 kiểu viêt cắt chuỗi
# q = q[0: 4]
# q = q[: 4]
# q = q[None: 4]
# print(q)
# sao chép chuỗi
# o = q[:]
# print(o)

# Cắt chuỗi có bước nhảy <chuỗi>[vị trí bắt đầu : vị trí dừng : bước]
# p = "nguaenmoa n   "
# p = p[0:15:1]
# p = p[0:100:3]
# print(p)

# 8.3.3 Ép kiểu dữ liệu
# a = '69'
# b = 69
# c = 69.0
# d = int(a)
# e = str(b)
# print(type(a))
# print(type(b))
# print(type(c))
# print(type(e))

# 8.3.4 Thay đổi nội dung chuỗi
# b = 'Xn chao '
# a = 'The gioi python'
# c = b + a
# d = hash(c)
# print(c)
# print(d)

# Chuỗi thứ nhất
# str1 = 'Haizz, \neu mot ngay nao do'
#
# # Chuỗi thứ hai
# str2 = 'Haizz, \neu mot ngay nao do'
#
# # In ra mã ASCII của từng ký tự trong từng chuỗi để so sánh
# print([ord(c) for c in str1])
# print([ord(c) for c in str2])
#
# # Kiểm tra xem hai chuỗi có giống nhau không
# print(str1 == str2)
#  BTVN
# s = "abc xyz"
# Đáp án nào in ra chuỗi rỗng?
# print(s[:])
# print(s[len(s):]) #rỗng vi tri bat dau lon hon vi tri ket thuc
# print(s[1:1]) #rỗng vi vi tri bat dau = vi tri ket thuc
# print(s[0::-1])  # giải vì sao khong in ra chuỗi rỗng
# In your case, s[0::-1] means start at index 0, stop at the beginning of the string (since no stop index is provided and the step is negative), and step backwards by 1. So, it starts at the first character of the string s which is 'a' and then tries to step backwards. But since 'a' is the first character, there's nowhere to step back to, so it stops there.  That's why it prints 'a' instead of an empty string. If you want to reverse the entire string, you should use
# print(s[0:0:-1]) #rỗng vi vi tri bat dau = 0, vi tri ket thuc = 0
